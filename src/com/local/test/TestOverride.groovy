package com.local.test

class TestOverride extends TestOverridesClazz {
    TestOverride(def pipeline) {
        super(pipeline)
        this.agentDataFileLinux = 'agentDataFileLinuxOverride'
    }

    @Override
    String hierarhyOverrideTest() {
        return "OverridedFromLocal"
    }
}
