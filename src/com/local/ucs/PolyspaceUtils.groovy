package com.local.ucs

class PolyspaceUtils extends PolyspaceUtilsClazz {
    PolyspaceUtils(def pipeline) {
        super(pipeline)
        this.polyspaceAccessAddress = "polyspace.local.com"
        this.maxParallelBugFinder = 3
        this.maxParallelCodeProver = 2
    }
}
