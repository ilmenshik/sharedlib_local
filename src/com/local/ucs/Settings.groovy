package com.local.ucs

import com.local.common.SettingsClazz

class Settings extends SettingsClazz {
    Settings(def pipeline) {
        super(pipeline)
        this.gitCredID = 'jenkins-ldap'
    }
}
