package com.local.ucs

class UserNotifications extends UserNotificationsClazz {
    UserNotifications(def pipeline) {
        super(pipeline)
    }

    @Override
    List<String> getAdditionalRecipients(String branchType) {
        List<String> recipients = []
        
        if (branchType == "debug") {
            echo "[DEBUG] Additional debug"
            recipients.add('me@debug.local')
        }

        return recipients
    }
}
